let express = require('express');
let path = require('path');
let bodyParser = require('body-parser');
let app = express();

//routes
let index = require('./api/routes/index');
let eventos = require('./api/routes/random');




//Para renderizar html
app.set( 'views', path.join(__dirname, './api/views') );
app.set( 'views engine', 'ejs' );
app.engine('html', require('ejs').renderFile);

//Para los models de la aplicacion
app.use(express.static(path.join(__dirname,'models')));

//Body Parser MW
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

//cors de la aplicacion
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});



//Se agrega en la raiz la documentacion
app.set("view options", {layout: false});
app.use(express.static(__dirname + '/doc'));

app.get('/', function(req, res) {
    res.render('index.html');
});

//Definir rutas y sus controladores
app.use('/api', eventos);





//Levantamos el componente
if(process.argv[2]!=null && process.argv[3]!=null){
	//Levantar el Servidor
	let port = parseInt(process.argv[2]);
	global.componente = process.argv[3];
	app.listen(port, function(){
		console.log('Servidor escuchando en el puerto ' + port +" el componente "+ componente);
	});

}else{
	console.log('Faltan parametros puerto y nombre de componente');	
	console.log('Ejecute nuevamente com por ejemplo: npm start 3000 "Base de Datos"');	
}
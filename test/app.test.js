const request = require('supertest')
const app = require('../api/routes/app')

describe('app', function() {
  it('respond with html', function(done) {
    request(app)
      .get('/')
      .expect('Content-Type', /html/)
      .expect(200, done);
  });
});
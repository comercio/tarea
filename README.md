# Tare 1 Para Comercio Electr�nico

En este proyecto se calcula un numero random del 0 al 1 que nos indica la disponibilidad de un componente del sistema.

## Getting Started

Estas son las instrucciones que deberias de seguir para poder copiar y correr correctamente tu proyecto en tu m�quina local para desarrollo y pruebas. Revisa desarrollo para m�s observaciones sobre como desplegar el proyecto.

### Prerequisites

Que tencnologias se necesitan tener instaladas antes de comenzar con el desarrollo.

```
Git
Node JS v6.9.2
```

### Installing

Para poder instalar el codigo tendremos que seguir los siguientes comandos:

```
git clone https://:GIT_USER@bitbucket.org/comercio/tarea.git
cd tarea/
npm install
npm start :PORT :SERVICE_NAME
```

## Running the tests

Entrar a la url http://localhost:PORT en ella se daa una breve descripcion de como probar las apis.

## Authors

* **Jos� Valentin Salina**
* **Pedro**

## License

This project is licensed under the MIT License
